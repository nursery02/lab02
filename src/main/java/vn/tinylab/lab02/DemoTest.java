package vn.tinylab.lab02;

import org.apache.log4j.Logger;
import vn.tinylab.lab02.model.TestCase;
import vn.tinylab.lab02.model.TestStep;
import vn.tinylab.lab02.model.TestSuite;

public class DemoTest {
    public static final Logger logger = Logger.getLogger(DemoTest.class);

    public static void main(String[] args) {
        logger.info("CREATE TEST SUITE");
        TestSuite  authTestsuite = new TestSuite(1001, "Authentication", "Verify input for login form");

        logger.info("CREATE TEST CASE");
        TestCase blankFormCase1 = new TestCase(10011, "Blank form case 1", "Blank username and blank password");
        TestStep case1Step1 = new TestStep(100111, "Blank form", "Press button without input form");
        blankFormCase1.setTestSteps(case1Step1);

        TestCase blankFormCase2 = new TestCase(10012, "Blank form case 2", "Blank username and valid password");
        TestStep case2Step1 = new TestStep(100121, "Input password", "Input password");
        TestStep case2Step2 = new TestStep(100122, "Press button ", "Press button");
        blankFormCase2.setTestSteps(case2Step1, case2Step2);

        TestCase blankFormCase3 = new TestCase(10013, "Blank form case 3", "Valid username and blank password");
        TestStep case3Step1 = new TestStep(100131, "Input username", "Input username");
        TestStep case3Step2 = new TestStep(100132, "Press button", "Press button");
        blankFormCase3.setTestSteps(case3Step1, case3Step2);

        logger.info("ADDING TEST CASE INTO TESTSUITE");
        authTestsuite.setTestCases(blankFormCase1, blankFormCase2, blankFormCase3);

    }
}
