package vn.tinylab.lab02.model;

import java.util.ArrayList;
import java.util.List;

public class TestSuite extends AbstractTest {
    private int verbose;
    private List<TestCase> testCases;

    public TestSuite(long id, String name, String description) {
        super(id, name, description);
        testCases = new ArrayList<>();
    }

    public TestSuite(long id, String name, String description, int verbose, List<TestCase> testCases) {
        super(id, name, description);
        this.verbose = verbose;
        this.testCases = testCases;
    }

    public int getVerbose() {
        return verbose;
    }

    public void setVerbose(int verbose) {
        this.verbose = verbose;
    }

    public List<TestCase> getTestCases() {
        return testCases;
    }

    public void setTestCases(List<TestCase> testCases) {
        this.testCases = testCases;
    }

    public void setTestCases(TestCase... cases) {
        for(TestCase tc: cases) {
            testCases.add(tc);
        }
    }
}
