package vn.tinylab.lab02.model;

import java.util.ArrayList;
import java.util.List;

public class TestCase extends AbstractTest {
    private List<TestStep> testSteps;

    public TestCase(long id, String name, String description) {
        super(id, name, description);
        testSteps = new ArrayList<>();
    }

    public TestCase(long id, String name, String description, List<TestStep> testSteps) {
        super(id, name, description);
        this.testSteps = testSteps;
    }

    public List<TestStep> getTestSteps() {
        return testSteps;
    }

    public void setTestSteps(List<TestStep> testSteps) {
        this.testSteps = testSteps;
    }

    public void setTestSteps(TestStep... steps) {
        for(TestStep ts: steps) {
            testSteps.add(ts);
        }
    }
}
